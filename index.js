const express = require("express");
const port = 4000;
const app = express();

app.use(express.json());

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];

app.get("/", (request, response) => {
            response.status(201).send("Page not available, please proceed to homepage.")
        })

app.get("/home", (request, response) => {
            response.status(201).send("Welcome to our page!")
        })


app.get("/items", (request, response) =>{
            response.send(items);
        })

app.delete("/delete-item", (request, response)=>{
            let deleteItem = items.pop();
            response.send(deleteItem);
        })



app.listen(port, ()=> console.log("Server is running at port 4000"))